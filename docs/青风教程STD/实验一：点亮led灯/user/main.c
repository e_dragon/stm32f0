/******************** (C) COPYRIGHT 2012 青风电子 ********************
 * 文件名  ：main
 * 描述    ：         
 * 实验平台：青风stm32f030开发板
 * 描述    ：LED闪烁
 * 作者    ：青风
 * 店铺    ：qfv5.taobao.com
**********************************************************************/

#include "stm32f0xx.h"
#include "led.h"

/**********************************************/
/* 函数功能；简单的延迟函数                   */
/* 入口参数：无                               */
/**********************************************/
void delay()
{
	int i,j;
  for(i=0;i<1000;i++)
	  {
			for(j=0;j<1000;j++);
		}
}
/**********************************************/
/* 函数功能；主函数                           */
/* 入口参数：无                               */
/**********************************************/
int main(void)
{
	LED_Init();
	while(1)
	{
		LED_Open();//打开led灯
		delay();
		LED_Close();//关掉led灯
    delay();		
	}
}
